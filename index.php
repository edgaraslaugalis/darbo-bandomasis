<!DOCTYPE html>
<html lang="en">
<head>
    <title>Roman to Integer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <h2>Roman to Integer</h2>
    <p>
        <?php if(isset($_POST['submit'])){
            $answer = romanToInteger($_POST['number']);
            if($answer){
                echo $answer;
            }else{
                echo "Incorrect roman numeral";
            }
        } ?>
    </p>
    <form action="" method="post">
        <div class="form-group">
            <label for="usr">Roman numeral:</label>
            <input type="text" class="form-control" name="number">
        </div>
        <input type="submit" name="submit" value="Convert">
    </form>
</div>

</body>
</html>

<?php

function romanToInteger($roman)
{
    $romans = array(
        'M' => 1000,
        'D' => 500,
        'C' => 100,
        'L' => 50,
        'X' => 10,
        'V' => 5,
        'I' => 1,
    );

    $result = 0;
    $chars = str_split($roman);
    foreach ($chars as $i => $char) {
        $next_char = end($chars);
        $third_char = end($chars);
        if (!empty($roman[$i + 1])) {
            $next_char = $roman[$i + 1];
            $third_char = $next_char;
        }
        if (!empty($roman[$i + 2])) {
            $third_char = $roman[$i + 2];
        }
        $current_char = $roman[$i];
        if ($romans[$current_char] >= $romans[$next_char] && $romans[$current_char] >= $romans[$third_char]) {
            $result += $romans[$current_char];
        } else {
            if ($romans[$current_char] == $romans[$next_char] / 10 || $romans[$current_char] == $romans[$next_char] / 5
            ) {
                $result -= $romans[$current_char];
            } else {
                return false;
            }
        }
    }
    return $result;
}
?>
